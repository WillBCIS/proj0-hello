# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

Author: 
William Bucher

Contact: 
wbucher@uoregon.edu

Description:
This is a simple hello world test. Part of a larger project to set up various 
necessary softwares, i.e. docker, bitbucket, git, etc.